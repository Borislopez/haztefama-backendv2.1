const express = require('express');
const app = express();

const chatPostulacionCtrl = require('../controller/mensajesPostulaciones.controller');


app.get('/postulacion/:id', chatPostulacionCtrl.getMensajesByPostulacionId);

app.post('/postulacion/nuevo',chatPostulacionCtrl.nuevoMensajeByPosutulacionId)

module.exports = app;