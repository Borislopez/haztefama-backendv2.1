const express = require('express');
const app = express();

const proveedoresFavCtrl = require('../controller/proveedorFav.controller');
const proveedorFavCtrl = require('../controller/proveedorFav.controller');

app.post('/favoritos', proveedoresFavCtrl.getByUserId);

app.post('/nuevofav', proveedorFavCtrl.newProveedorFav);

app.delete('/:id/:idUser', proveedorFavCtrl.deleteById);

module.exports = app;