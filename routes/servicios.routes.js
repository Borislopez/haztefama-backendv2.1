const express = require('express');
const app = express()

const serviciosCtrl = require('../controller/servicios.controller');

app.get('/servicio/:id', serviciosCtrl.getById);
app.post('/finalizado', serviciosCtrl.getFinalized);

app.get('/servicio', serviciosCtrl.getAll);
app.get('/servicioAdmin', serviciosCtrl.getAllAdmin);
app.get('/usuario/:id', serviciosCtrl.getAllbyUser);

app.post('/serviciofilter', serviciosCtrl.getAllFilter);
app.post('/serviciofilteruser', serviciosCtrl.getUserServicesFilter);

app.post('/pagarProveedor',serviciosCtrl.pagarProveedor)

app.post('/servicio', serviciosCtrl.new);

app.put('/servicio/:id', serviciosCtrl.updateById);

app.post('/servicio/cambiar-status', serviciosCtrl.updateStatusById);

app.delete('/servicio/:id/:idAdmin', serviciosCtrl.daleateById);

module.exports = app;
