const Postulaciones = require('../models/postulaciones');
const Usuario = require('../models/usuario');
const usuario = require('../models/usuario');
const Notificaciones = require('../models/notificaciones');
const io = require("../socket");

const postulacionesCtrl = {};


postulacionesCtrl.postulacionesPorServicio = async (req,res)=>{
    Postulaciones.find({servicio:req.params.id,status:{$in:[1,2]}})
        .populate('usuario servicio')

        .sort({fecha_creacion:-1})
        .exec((err,postulaciones)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                res.status(200).json({
                    ok:true,
                    postulaciones,
                    url:`${req.protocol}://${req.headers.host}/user/uploads/`
                })
            }
        })
}

postulacionesCtrl.usuarioPorPostulacionId = async (req,res) => {
    console.log(req.params.id);
    
    Postulaciones.findById(req.params.id)
        .populate('usuario')
        .exec((err,postulacion)=>{
            if (err) {
                res.status(200).json({
                    ok:false,
                    err
                })
            } else {
                res.status(200).json({
                    ok:true,
                    usuario:postulacion.usuario
                })
            }
        })
}

postulacionesCtrl.postulacionesPorUsuario = async (req,res)=>{
    Postulaciones.find({usuario:req.params.id})
        .populate('usuario servicio')
        .populate({path:'servicio',populate:{path:'categoria subcategoria usuario'}})

        .sort({fecha_creacion:-1})
        .exec((err,postulaciones)=>{
            if(err){
                res.status(400).json({
                    ok:false,
                    err
                })
            }else{
                res.status(200).json({
                    ok:true,
                    postulaciones,
                    url:`${req.protocol}://${req.headers.host}/user/uploads/`
                })
            }
        })
}


postulacionesCtrl.nuevaPostulacion = async(req,res) => {
 
    Postulaciones.findOne({usuario:req.body.usuario,servicio:req.body.servicio._id})
        .exec((err,postulacion)=>{
            if (err) {
                res.status(200).json({
                    ok:false,
                    err
                })
            } else {
                if(postulacion == null){
                    Usuario.findById(req.body.usuario)
                    .exec((err,usuario)=>{
                        if(err){
                            res.status(200).json({
                                ok:false,
                                err
                            })
                        }else{
                        
                            
                            if(usuario.creditos < req.body.creditos){
                                res.status(200).json({
                                    ok:false,
                                    err:{msg:'No posee suficiente creditos',code:10}
                                })
                            }else{
                                Usuario.findByIdAndUpdate(usuario._id,{$inc:{creditos:-req.body.creditos}})
                                    .exec((err,usuario)=>{
                                        if(err){
                                            res.status(200).json({
                                                ok:false,
                                                err
                                            })
                                        }else{
                                            Postulaciones.create({
                                                usuario:usuario._id,
                                                servicio:req.body.servicio._id
                                            }).then((postulacion)=>{
                                                Notificaciones.create({
                                                    usuario:req.body.servicio.usuario._id,
                                                    mensaje:'Nueva postulación de un proveedor a tu servicio',
                                                    link:`/cliente/servicio/postulaciones`,
                                                    parametros:{id:req.body.servicio._id}
                                                })
                                              
                                                res.status(200).json({
                                                    ok:true,
                                                    postulacion
                                                })
                                            }).catch((err)=>{
                                                res.status(200).json({
                                                    ok:false,
                                                    err
                                                })
                                            })
                                        }
                                    })
                            }
                        }
                    })
                }else{
                    res.status(200).json({
                        ok:false,
                        err:{msg:'Ya tienes una postulacion en este servicio',code:20}
                    })
                }
                
            }
        })
   
    
}

postulacionesCtrl.servicioPorPostulacion = async(req,res) => {
    Postulaciones.findById(req.params.id)
        .populate({path : 'servicio', populate : {path : 'usuario'}})
        .exec((err,postulacion)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                res.status(200).json({
                    ok:true,
                    servicio:postulacion.servicio
                })
            }
        })
    
}

module.exports = postulacionesCtrl