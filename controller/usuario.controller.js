process.env.NODE_TLS_REJECT_UNAUTHORIZED='0'

var Usuario = require('../models/usuario');
var nodemailer = require('nodemailer');
var bcrypt = require('bcryptjs');
var Bitacora = require('../models/bitacora')
var Categoria = require('../models/categoria')
var multer  =   require('multer');
var fs = require('fs');
var path = require('path');
var ObjectId = require('mongodb').ObjectID;
var UsuarioSubcategoria = require('../models/usuario_subcategorias')

const usersSocket = require('../userSocket')
const io = require('../socket');
const bitacora = require('../models/bitacora');


if(process.argv.indexOf('--prod') !== -1){  
    var URL_WEB = `http://64.227.31.7/apphaztefama/#`
}else{
    var URL_WEB = `http://localhost:4200/#`
}

io.on('connection',(socket) =>{

    console.log('se conecto',socket.id);


    
    socket.on('registerID',(data)=>{usersSocket.pushUser({id:socket.id,userId:data.id})})
    

    socket.on('logAdmin',(data)=>{
        
        
        socket.join('administradores')
    })


    socket.on('sala_chat',(data)=>{
        socket.join(data.name_room)
        
    })

  
    
    socket.on('nuevo_mensaje',(data)=>{
        console.log(data);
        
        io.to(`${data.name_room}`).emit('nuevo_mensaje_r')
    })
    
 

    socket.on('cambios_en_datos',(data)=>{
        usersSocket.getUserByIdUser(data.to).then((usuario)=>{  
            console.log('esto',usuario)
            io.to(usuario.id).emit('cambios_en_datos_r',{})
            
        })
    })

    socket.on('disconnect', function() {
        usersSocket.popUserById(socket.id)
       
    });
    
})

const usuarioCtrl = {};

usuarioCtrl.newUser = (req,res)=>{


    let body = req.body
    body.password =  bcrypt.hashSync(body.password,10)
    body.simpleAccount =  true

    body.token_verificacion = tokenNumber()

    let admin = new Usuario(body)
    
    
    admin.save(async (err,userSaved)=>{
        if(err){
            console.log('save error' + err);
            
           
            if(err.code == 11000){            
        
                
                let index = await returnIndex(err.keyPattern)
                res.status(200).json({
                    ok: false,
                    msg: 'Ocurrio un error inesperado.',
                    err: err,
                    index
                })
            }else{
                res.status(200).json({
                    ok: false,
                    msg: 'Ocurrio un error inesperado.',
                    err: err
                })
            }
            
        }else{

            if(req.body.role == 'admin'){
                Bitacora.create({
                    usuario:req.body.idAdmin,
                    accion:`Registro al administrador ${userSaved.username}`
                }) 
            }else{

                Bitacora.create({
                    usuario:userSaved._id,
                    accion:'Se registro en el sistema'
                })
            }

          
        verificationEmail(userSaved.token_verificacion,userSaved)
         res.status(200).json({
            ok: true,
            msg: 'El admin se ha guardado.',
            user: userSaved
        })
        
        }

       
    })
}

function returnIndex(keyP){
for (const key in keyP) {
    return key
    
}

    
}

function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}


usuarioCtrl.newSocialAccount = (req,res)=>{

    let body = req.body
  
    body.username = `${body.nombre}_${body.apellido}_${randomString(5, '0123456789')}`
    let admin = new Usuario(body)
    

    admin.save((err,userSaved)=>{
        if(err){
            res.status(200).json({
                ok: false,
                msg: 'Ocurrio un error inesperado.',
                err: err
            })
        }else{
            Bitacora.create({
                usuario:userSaved._id,
                accion:'Se registro en el sistema usando las redes sociales'
            })
         res.status(200).json({
            ok: true,
            msg: 'El admin se ha guardado.',
            user: userSaved
        })
        console.log(userSaved)
        }

       
    })
}


usuarioCtrl.updatePosition = async (req,res) => {
    console.log( req.body.ubicacion);
    
    Usuario.findByIdAndUpdate(req.params.id,{ubicacion:JSON.stringify(req.body.ubicacion)})
        .exec((err,usuario)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                res.status(200).json({
                    ok:true,
                    usuario
                })
            }
        })
}

usuarioCtrl.resendEmailVerify = (req,res) =>{
    let token = tokenNumber()
    Usuario.findByIdAndUpdate(req.body.id,{token_verificacion:token})
        .exec((err,user)=>{
            if(err){
                res.status(200).json({
                    ok: false,
                    msg: 'Ocurrio un error inesperado.',
                    err: err
                })
            }else{
                if(user){
                    console.log(user)
                    Bitacora.create({
                        usuario:req.body.id,
                        accion:'Reenvio el correo de verificación'
                    })
                    verificationEmail(token,user)
                    res.status(200).json({
                        ok: true,
                        msg: 'mensaje reenviado',
                        user
                    })
                }else{
                
                    res.status(200).json({
                        ok: false,
                        msg: 'Ocurrio un error inesperado.',
                        err: err
                    })
                }
            }
        })
}


function tokenNumber() {
    return 'xxxxxxxxxxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);      
      return v.toString(16);
    });
}

function verificationEmail(token,user){
    let url = `${URL_WEB}/verificar?token=${token}`
  
    
    var transporter = nodemailer.createTransport({
        host: 'mail.proyectokamila.com',
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: 'boris.lopez@proyectokamila.com', // generated ethereal user
            pass: 'Temporal01' // generated ethereal password
          }
       });

    const mailOptions = {
        from: 'boris.lopez@proyectokamila.com', // sender address
        to: `${user.email}`, // list of receivers
        subject: 'Haztefama', // Subject line
        html: `  
       <h2 class="text-center">Verificar correo</h2>
    
       
         <p style="margin-top: 7px;">
             Estimado ${user.nombre}
             Para poder terminar con su registro por favor haga click en el siguiente enlace
         </p>
         
         <a href=${url}>${url}</a>
         
 
     <p>Equipo PK.</p>  
         <p>
           Esta es una cuenta de correo no monitoreada, por favor no responda o reenvie a este correo electrónico.
       
         </p>
      
   
   </div>
  </p>`// plain text body
      };

      transporter.sendMail(mailOptions, function (err, info) {
        if(err){
            setTimeout(() => {
                verificationEmail(token,user)
            }, 3000);
        }
          
        else{
        
        console.log(info);
        
        }
          
     });
}


usuarioCtrl.getUsers = (req,res)=>{
    Usuario.find({borrado:false})
    .exec((err,usuarios)=>{

        if (err) {
            res.status(200).json({
                ok:false,
                err
            })
        } else {
            res.status(200).json({
                ok:true,
                usuarios
            })
        }
    })
}


usuarioCtrl.getUserById = (req,res)=>{
   
    Usuario.findById(req.params.id)
    .populate('categoria')
    .exec((err,data)=>{
        if(err){
            res.status(200).json({
                ok:false,
                err
            })
        }else{
            if(data != null){
              
                
               
                 res.status(200).json({
                    ok:true,
                    data,
                    url:`${req.protocol}://${req.headers.host}/user/uploads/`
                })
            }else{
                res.status(200).json({
                    ok:false,
                    err:{code:404}
                })
            }
        }
       
    })
}

usuarioCtrl.getUserByRole = (req,res)=>{
   
    Usuario.find({role : req.params.role, borrado:false})
    .populate('categoria subcategoria')
    .exec((err,data)=>{
        if(err){
            res.status(200).json({
                ok:false,
                err
            })
        }else{
            if(data != null){                
               
                 res.status(200).json({
                    ok:true,
                    data,
                    url:`${req.protocol}://${req.headers.host}/user/uploads/`
                })
            }else{
                res.status(200).json({
                    ok:false,
                    err:{code:404}
                })
            }
        }
       
    })
}

usuarioCtrl.getUserByRoleCategory = (req,res)=>{
    console.log(req.body)
   let conditions = {
    role : req.body.tipo,
    borrado:false
   }
   if (req.body.categoria.length > 0) {
    conditions.categoria = { $in: req.body.categoria };
  }
  let or = [{}];

    Usuario.find(conditions)
    .populate('categoria')
    .or(or)
    .exec((err,data)=>{
        if(err){
            res.status(200).json({
                ok:false,
                err
            })
        }else{
            if(data != null){                
               
                 res.status(200).json({
                    ok:true,
                    data,
                    url:`${req.protocol}://${req.headers.host}/user/uploads/`
                })
            }else{
                res.status(200).json({
                    ok:false,
                    err:{code:404}
                })
            }
        }
       
    })
}
usuarioCtrl.getUserByCategory = (req,res)=>{
    // console.log(req.params.category);
    const code = req.params.category;
    console.log(code);
    
    Categoria.findOne({ code : code })
        .exec((err, category) =>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }
            console.log(category);
            Usuario.find({ categoria : category._id , borrado : false })
            .exec((err,data)=>{
                if(err){
                    res.status(200).json({
                        ok:false,
                        err
                    })
                }else{
                    if(data != null){                
                       
                         res.status(200).json({
                            ok:true,
                            proveedores: data,
                            category: category,
                            url:`${req.protocol}://${req.headers.host}/user/uploads/`
                        })
                    }else{
                        res.status(200).json({
                            ok:false,
                            err:{code:404}
                        })
                    }
                }
               
            })    
        });
   
}

usuarioCtrl.updateRolAdminById = (req,res)=>{

    req.body.fecha_modificacion = Date.now();

    Usuario.findByIdAndUpdate(req.params.id ,req.body)
     .exec((err,usuario)=>{
         if(err){
             res.status(200).json({
                 ok:false,
                 err
             })
         }else{
             Bitacora.create({
                 usuario:req.body.idAdmin,
                 accion:'Modifico sus datos de perfil'
             })
             res.status(200).json({
                 ok:true,
                 usuario
             })
         }
     })
 
 }

usuarioCtrl.newPassword = (req,res)=>{
    let id = req.params.id
    let body = req.body



    Usuario.findById(id)
        .exec((err,user)=>{
            if(err){
                res.status(200).json({
                    ok: false,
                    msg: 'ocurrio un error inesperado',
                    err: err
                })
            }else{
               
                if(bcrypt.compareSync(body.current_password,user.password)){
                    Usuario.findByIdAndUpdate(id,{password: bcrypt.hashSync(body.new_password,10)})
                        .exec((err,userModifyPassword)=>{
                            if(err){
                                res.status(200).json({
                                    ok: false,
                                    msg: 'ocurrio un error inesperado',
                                    err: err
                                })
                            }else{
                                userModifyPassword.password = ':)'
                                res.status(200).json({
                                    ok:true,
                                    msg: 'admin modificado',
                                    userModifyPassword
                                })
                            }
                        })
                }else{
                    res.status(200).json({
                        ok: false,
                        msg: 'la contraseña no coincide',
                        err: 2
                    })
                }

            }
        })

   
}

usuarioCtrl.updatePerfilById = (req,res)=>{
 
   Usuario.findByIdAndUpdate(req.params.id,req.body)
    .exec((err,usuario)=>{
        if(err){
            res.status(400).json({
                ok:false,
                err
            })
        }else{
            Bitacora.create({
                usuario:req.params.id,
                accion:'Modifico sus datos de perfil'
            })
            if(usuario.role == 'provider'){
                UsuarioSubcategoria.updateMany({usuario:usuario._id}, { $set: { borrado: true } })
                .exec((err,succes)=>{
                    if(err){
                        console.log(err);
                        
                    }else{
                        for (var subcategoria of req.body.subcategorias) {
                
                            let userSub = new UsuarioSubcategoria({usuario:usuario._id,subcategoria:subcategoria})
                            userSub.save((err,subcategoria)=>{
                             
                            })
                            
                                                 
                        }
                    }
                })
               
            }
            res.status(200).json({
                ok:true,
                usuario
            })
        }
    })

}

usuarioCtrl.updateSeguridadById = (req,res) => {
    console.log(req.body);
    

    Usuario.findById(req.params.id)
        .exec((err,usuario)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
               if(bcrypt.compareSync(req.body.password,usuario.password)){
                if(req.body.cambiar){
                    req.body.password = bcrypt.hashSync(req.body.npassword,10)
                }else{
                    req.body.password = bcrypt.hashSync(req.body.password,10)
                }
                  
                    Usuario.findByIdAndUpdate(req.params.id,req.body)
                      .exec((err,usuario)=>{
                          if(err){
                              res.status(400).json({
                                  ok:false,
                                  err
                              })
                          }else{
                              Bitacora.create({
                                  usuario:req.params.id,
                                  accion:'Modifico sus datos de seguridad'
                              })
                              res.status(200).json({
                                  ok:true,
                                  usuario
                              })
                          }
                      })
               


               }else{
                res.status(200).json({
                    ok:false,
                    err:{code:404}
                })
                   
               }
            }
        })

}


var storage =   multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './uploads');
    },
    filename: function (req, file, callback) {
        console.log(file);
        if(file.mimetype.split('/')[0] == 'image'){
            callback(null, `${Date.now()}${ path.extname(file.originalname)}` )

        }else{
            console.log(`${Date.now()}@${ file.originalname}`);
            
            callback(null, `${Date.now()}@${ file.originalname}` )
 
        }
        
    }
  });


  usuarioCtrl.getImageByName = async (req,res) => {
    fs.readFile('./uploads/' + req.params.image, function (err, content) {
        if (err) {
            res.writeHead(400, {'Content-type':'text/html'})
            console.log(err);
            res.end("No such image");    
        } else {
            //specify the content type in the response will be an image
            res.writeHead(200,{'Content-type':'image/jpg'});
            res.end(content);
        }
    });
 
  }

  usuarioCtrl.getDocumentByName = async (req,res) => {
      console.log(`./uploads/${req.params.document}`);
      
    res.download(`./uploads/${req.params.document}`,req.params.document.split('@')[1])
 
  }

usuarioCtrl.uploadImageById = async (req,res) => {
 
    
    var upload = multer({ storage : storage}).single('image');

    upload(req,res,function(err) {
        
        if(err) {
           
            
            return res.end("Error uploading file.");
        }else{
          
            
            res.status(200).json({
                ok:true,
                url:`${req.protocol}://${req.headers.host}/user/uploads/${req.file.filename}`,
                name:req.file.filename
            })
        }
       
    });
    
    
}



usuarioCtrl.Auth = (req,res)=>{
    let body = req.body

    Usuario.find({ "email": body.email,simpleAccount:true})
        .exec((err,data)=>{
            if (err){
                res.status(200).json({
                    ok: false,
                    msg: 'Ocurrio un error',
                    err: err
                })
            }
            else if(data[0]){
                if(bcrypt.compareSync(body.password,data[0].password)){
                    Bitacora.create({
                        usuario:data[0].id,
                        accion:'Ingreso al sistema'
                    })
                    res.status(200).json({
                        ok: true,
                        msg: 'Las credenciales son validas.',
                        role: data[0].role,
                        id: data[0].id,
                        verify: data[0].verificado,
                        email:data[0].email,
                        status:data[0].status
                    })
                }else{
                    res.status(200).json({
                        ok: false,
                        msg: 'Credenciales invalidas.',
                        err: null
                    })
                }
            }
            else{
                res.status(200).json({
                    ok: false,
                    msg: 'Credenciales invalidas.',
                    err: null
                })
            }
        });
}

usuarioCtrl.AuthSocialNetwork = (req,res)=>{

    Usuario.findOne({uid:req.body.uid})
    .exec((err,user)=>{
        if(err){
            res.status(200).json({
                ok: false,
                msg: 'ocurrio un error inesperado',
                err: err
            })
        }else{
        if(user){
            Bitacora.create({
                usuario:user._id,
                accion:'Ingreso al sistema mediante redes sociales'
            })
            res.status(200).json( {
                ok:true,
                user,
                status:user.status
            })
        }else{
            res.status(200).json( {
                ok:false,
                err:{code:404}
            })
        }
    

        }
    })

}

usuarioCtrl.deleteById = (req,res)=>{
    Usuario.findByIdAndUpdate(req.params.id,{borrado:true,fecha_modificacion:Date.now()})
    .exec((err,usuario)=>{
        if(err){
            res.status(200).json({
                ok:false,
                msg:`Hubo un error al intentar eliminar al usuario el id = ${req.params.id}`,
                err:err
            })
        }else{
            Bitacora.create({
                usuario:req.params.idAdmin,
                accion:`Borro al usuario ${usuario.username}`
            })
            res.status(200).json({
                ok:true,
                msg:'usuario eliminado',
                usuario
            })
        }
    })
}


usuarioCtrl.recoveryPassword = (req,res)=>{
    
    Usuario.findOne({email:req.body.email})
        .exec((err,user)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                if(user){
                    let token = tokenNumber()
                    Usuario.findByIdAndUpdate(user._id,{recovery_password: true,recovery_password_token:token})
                        .exec((err,user)=>{
                            Bitacora.create({
                                usuario:user._id,
                                accion:'Solicito un correo de olvido de contraseña'
                            })
                            recoveryPasswordEmail(token,user)
                            res.status(200).json({
                                ok:true,
                                user
                            }
                               
                            )
                        })
                   
                }else{
                    res.status(200).json({
                        ok:false,
                        user
                    })
                }
            }
        })
}

function recoveryPasswordEmail(token,user){
    let url = `${URL_WEB}/nueva-contrasena?token=${token}`
    var transporter = nodemailer.createTransport({
        host: 'mail.proyectokamila.com',
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: 'samuel.lizarraga@proyectokamila.com', // generated ethereal user
            pass: 'j27040372' // generated ethereal password
          }
       });

    const mailOptions = {
        from: 'samuel.lizarraga@proyectokamila.com', // sender address
        to: `${user.email}`, // list of receivers
        subject: 'Recuperar contraseña (Haztefama)', // Subject line
        html: `  
       <h2 class="text-center">Enlace para recupera contraseña</h2>
    
       
         <p style="margin-top: 7px;">
             Estimado ${user.nombre}
             Para poder recuperar su contraseña por favor haga click en el siguiente enlace
         </p>

         
         <a href=${url}>${url}</a>
         
    
 
     <p>Equipo Haztefama.</p>  
         <p>
           Esta es una cuenta de correo no monitoreada, por favor no responda o reenvie a este correo electrónico.
       
         </p>
      
   
   </div>
  </p>`// plain text body
      };

      transporter.sendMail(mailOptions, function (err, info) {
      
          
        if(err){
            console.log(err);
            setTimeout(() => {
                recoveryPasswordEmail(token,user) 
            }, 3000);
            
        }
          
        else{
        
        console.log(info);
        
        }
          
     });
}


usuarioCtrl.authRecoveryPassword = (req,res)=>{
    Usuario.findOne({recovery_password:true,recovery_password_token:req.body.token})
        .exec((err,user)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                
                if(user){
                    res.status(200).json({ok:true})
                }else{
                    res.status(200).json({ok:false})
                }
            }
        })
}

usuarioCtrl.setNewPassword = (req,res)=>{    
    Usuario.findOneAndUpdate({recovery_password_token:req.body.recovery_password_token},{password:bcrypt.hashSync(req.body.password,10),recovery_password:false})
        .exec((err,user)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                Bitacora.create({
                    usuario:user._id,
                    accion:'Recupero (Cambio) su contraseña mediante enlace'
                })
                
                res.status(200).json({
                    ok:true,
                    user
                })
            }
        })
}


usuarioCtrl.authEmail = (req,res) => {
    Usuario.findOne({token_verificacion:req.body.token})
        .exec((err,user)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                if(user){
                    Usuario.findByIdAndUpdate(user._id,{verificado:true,token_verificacion:null})
                        .exec((err,userModify)=>{
                            if(err){
                                res.status(200).json({
                                    ok:false,
                                    err
                                })
                            }else{
                                Bitacora.create({
                                    usuario:userModify,
                                    accion:'Verifico el correo electronico'
                                })
                                
                                res.status(200).json({
                                    ok:true,
                                    userModify
                                })
                            }
                        })
                   
                }else{
                    res.status(200).json({
                        ok:false,
                        err:{code:404}
                    })
                }
             
            }
        })
}

usuarioCtrl.updateStatusById = async (req,res) => {
    Usuario.findById(req.body.id)
    .exec((err,usuario)=>{
        if(err){
            res.status(200).json({
                ok:false,
                msg:`Hubo un error al intentar cambiar el estado a la usuario`,
                err:err
            })
        }else{
            
            Usuario.findByIdAndUpdate(usuario._id,{status:!usuario.status,fecha_modificacion: Date.now()})
            .exec((err,usuario)=>{
                if(err){
                    res.status(200).json({
                        ok:false,
                        msg:`Hubo un error al intentar cambiar el estado a la subategoria`,
                        err:err
                    })
                }else{
                    Bitacora.create({
                        usuario:req.body.idAdmin,
                        accion:`Cambio el estado del usuario ${usuario.username} a '${!usuario.status?'Activo':'Desactivo'}'`
                    })
                    res.status(200).json({
                        ok:true,
                        msg:'usuarios modificado',
                        usuario
                    })
                }
            })
        }
    })
}

usuarioCtrl.updateUserByAdmin = async (req,res) => {
    console.log(req.body);
    Usuario.findByIdAndUpdate(req.body.id,req.body)
        .exec((err,usuario)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                Bitacora.create({
                    usuario:req.body.idAdmin,
                    accion:` Modifico los datos de el usuario ${usuario.username}'`
                })
                res.status(200).json({
                    ok:true,
                    msg:'usuario modificado',
                    usuario
                })
            }
        })
}




module.exports = usuarioCtrl;