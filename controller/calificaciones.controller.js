const Calificacion = require('../models/calificacion');
const Bitacora = require('../models/bitacora');
const usersSocket = require('../userSocket');
const io = require('../socket');
const Usuario = require('../models/usuario');

io.on("connection", (socket) => {
    socket.on("calificacion_update", () => {
        socket
        .to("administradores")
        .emit("calificacion_update_r", "calificacion actualizado");
    });

    socket.on("comentario_cliente", (data) => {
        usersSocket.getUserByIdUser(data.to).then((usuario) => {
        io.to(usuario.id).emit("calificacion_update_r", { msg: "cambios" });
        });
    });

    socket.on("cambios_en_datos", (data) => {
        usersSocket.getUserByIdUser(data.to).then((usuario) => {
        io.to(usuario.id).emit("cambios_en_datos_r", {});
        });
    });

    socket.on("disconnect", function () {
        usersSocket.popUserById(socket.id);
    });
});

const calificacionesCtrl = {};

calificacionesCtrl.getAll = async (req, res) => {
    let conditions = {
        borrado: false,
    };

    Calificacion.find(conditions)
        .populate('usuario')
        .sort({fechaCreacion: -1})
        .exec((err, calificaciones) => {
            if (err) {
                res.status(200).json({
                    ok: false,
                    err
                })
            } else {
                res.status(200).json({
                    ok: true,
                    calificaciones,
                    url: `${req.protocol}://${req.headers.host}/user/uploads/`,
                });
            }
        })
}

calificacionesCtrl.newCalificacion = async (req, res) => {
    let calificacion = new Calificacion(req.body);
    calificacion.save((err, calificacion) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        Bitacora.create({
          usuario: req.body.usuario,
          accion: `Realizo una calificacion en el sistema`,
        });
        res.status(200).json({
          ok: true,
          calificacion,
        });
      }
    });
  };

calificacionesCtrl.getByUserID = async (req, res) => {
    Calificacion.find({ usuario: req.params.id, borrado: false })
    .sort({ fecha_creacion: -1 })
    .populate("usuario")
    .exec((err, calificacion) => {
        if (err) {
        res.status(200).json({
            ok: false,
            err,
        });
        } else {
        res.status(200).json({
            ok: true,
            comentarios,
            url: `${req.protocol}://${req.headers.host}/user/uploads/`,
        });
        }
    });
};

calificacionesCtrl.borrarCalificacion = async (req, res) => {
    Calificacion.findByIdAndUpdate(req.params.id, { borrado: true })
      .populate("usuario")
      .exec((err, calificacion) => {
        if (err) {
          res.status(200).json({
            ok: false,
            err,
          });
        } else {
          Bitacora.create({
            usuario: req.params.idAdmin,
            accion: `Marco como inapropiado un comentario de ${comentario.usuario.username}`,
          });
          res.status(200).json({
            ok: true,
            calificacion,
          });
        }
      });
};

calificacionesCtrl.promedioCalificacionByUserId = async (req, res) => {
    let conditions = {
        usuario: req.params.id,
        borrado:false,
    };

    Calificacion.find(conditions)
        .sort({fechaCreacion: -1})
        .exec((err, calificaciones) => {
            console.log(calificaciones);
            if (err) {
                res.status(200).json({
                    ok: false,
                    err
                })
            } else {
                res.status(200).json({
                    ok: true,
                    calificaciones,
                    url: `${req.protocol}://${req.headers.host}/user/uploads/`,
                });
            }
        })
};

module.exports = calificacionesCtrl;