const Bitacora = require('../models/bitacora');
const ChatPostulacion = require('../models/chatPostulaciones')
const Notificaciones = require('../models/notificaciones')
const usersSocket = require("../userSocket");
const io = require("../socket");
const mensajesPostulacionCtrl = {};

mensajesPostulacionCtrl.getMensajesByPostulacionId = async(req,res) => {

    ChatPostulacion.find({postulacion:req.params.id})
        .sort({fecha_creacion:1})
        .populate('usuario postulacion')
        .exec((err,chat)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                res.status(200).json({
                    ok:true,
                    chat,
                    url:`${req.protocol}://${req.headers.host}/user/uploads/`,
                    urlDownload:`${req.protocol}://${req.headers.host}/user/documentuploads/`,
                })
            }
        })

}

mensajesPostulacionCtrl.nuevoMensajeByPosutulacionId = async(req,res) => {
   
    
    let mensaje = new ChatPostulacion(req.body.mensaje)
    console.log(req.body)
    mensaje.save((err,mensaje)=>{
        if(err){
            res.status(200).json({
                ok:false,
                err
            })
        }else{
            Notificaciones.create({
                usuario:req.body.cliente,
                mensaje:'Nuevo mensaje recibido de un servicio',
                link:req.body.link,
                parametros:{id:req.body.mensaje.postulacion}
            })
            res.status(200).json({
                ok:true,
                mensaje
            })
        }
    })
}


module.exports = mensajesPostulacionCtrl