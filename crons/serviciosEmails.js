const emailsServicios = require('../models/solicitudServicioEmails')
if(process.argv.indexOf('--prod') !== -1){  
    var URL_WEB = `http://64.227.31.7/apphaztefama/#`
}else{
    var URL_WEB = `http://localhost:4200/#`
}

var nodemailer = require('nodemailer');
var mailer = nodemailer.createTransport({
    host: 'mail.proyectokamila.com',
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
        user: 'samuel.lizarraga@proyectokamila.com', // generated ethereal user
        pass: 'j27040372' // generated ethereal password
      }
   });

var hbs = require('nodemailer-express-handlebars');
const { update } = require('../models/solicitudServicioEmails');
var options = {
viewEngine : {
    extname: '.hbs', // handlebars extension
    layoutsDir: 'views/email/', // location of handlebars templates
    defaultLayout: 'template', // name of main template
    partialsDir: 'views/email/', // location of your subtemplates aka. header, footer etc
},
viewPath: 'views/email',
extName: '.hbs'
};

nuevaSolicitudEmail = async (usuario,solicitud,id) =>{
    mailer.use('compile', hbs(options));
    mailer.sendMail({
        from: 'samuel.lizarraga@proyectokamila.com',
        to: usuario.email,
        subject: `Solicitud de servicio en tu categoria`,
        template: 'template',
        context: {
            firstName: usuario.nombre,
            lastName: usuario.apellido,
            link:`${URL_WEB}/proveedor/servicio/informacion?id=${solicitud._id}`,
            categoria:usuario.categoria.nombre,
            descripcion:solicitud.descripcion,
            lugar:solicitud.lugar_evento
            
        }
        }, (error, response)=>{
            if(error){
            
                
            }else{
                console.log(response);
                emailsServicios.findByIdAndUpdate(id,{enviado:true})
                    .exec((err,update)=>{
                        console.log('Mensaje enviado');
                        
                        
                    })
            }
           
            
        }) 

}

module.exports = (colaEmails = (obj) =>{

    
    setInterval(() => {
     
        
        emailsServicios.find({enviado:false})
        .populate('usuario servicio')
        .exec((err,emails)=>{
            if(err){
               
                
            }else{
               
                
               emails.forEach((email)=>{
                nuevaSolicitudEmail(email.usuario,email.servicio,email._id)
               })
                
            }
        })
    }, 1000 * 60 * 5);

    
    
})();