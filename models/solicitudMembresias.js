
var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var solicitudMembresiaSchema = new Schema({
    usuario: { type: String, required: true , ref: 'usuario'},
    membresia: { type: String, required: true , ref: 'membresia'},
    order:{type:String, required:true,unique:true},
    fecha_creacion: {type: Date, default: Date.now }
});

module.exports = mongoose.model('solicitudmembresia', solicitudMembresiaSchema);