var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var contizacionSchema = new Schema({
    postulacion:{type:Schema.Types.ObjectId,required:true,ref:'postulaciones'},
    contratoStatus:{type: Number, default: 0},
    servicio:{type:Schema.Types.ObjectId,required:true,ref:'servicios'},
    fecha_creacion: {type: Date, default: Date.now },
    motivo:{type:String, required:true},
    monto: {type: Number, default: 0 },
    status:{type:Number, default:1},
    borrado:{type:Boolean, default:false},
    id_proveedor:{type:Schema.Types.ObjectId,required:true,ref:'usuario'},
    id_cliente:{type:Schema.Types.ObjectId,required:true,ref:'usuario'},
    porcentajeCancelado: {type: Number, default: 0},
    montoPagado: {type: Number, default: 0}
});
// contratoStatus
// 0 - Rechazada
// 1 - cliente
// 2 - aceptado
 
// status
// 0 - Rechazada
// 1 - Enviada
// 2 - Aceptada 
// 3 - Pagada


module.exports = mongoose.model('cotizaciones', contizacionSchema);