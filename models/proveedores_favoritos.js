
var mongoose = require('mongoose');


var Schema = mongoose.Schema;


var proFavSchema = new Schema({
    cliente: { type: Schema.Types.ObjectId, required: true , ref: 'usuario'},
    proveedor: { type: Schema.Types.ObjectId ,required:true, ref:'usuario'},
    categoria: { type: Schema.Types.ObjectId ,required:true, ref:'categoria'},
    fecha_creacion: {type: Date, default: Date.now },
    borrado:{type:Boolean,default:false},
    subcategoria: { type: Schema.Types.ObjectId , ref: 'usuario_subcategoria'},
});

module.exports = mongoose.model('proveedores_favoritos', proFavSchema);