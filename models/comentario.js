var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var comentarioSchema = new Schema({
  usuario: { type: String, required: true, ref: "usuario" },
  comentario: { type: String },
  fecha_creacion: { type: Date, default: Date.now },
  visto: { type: Boolean, default: false },
  borrado: { type: Boolean, default: false },
  respuesta_comentario: { type: Object },
  respuesta_borrado: { type: Boolean, default: true }
});

module.exports = mongoose.model("comentario", comentarioSchema);
