const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let serviciosSchema = new Schema({

    usuario: { type: Schema.Types.ObjectId, ref:"usuario",  required: true},
    servicio: { type: Schema.Types.ObjectId, ref:"servicios",  required: true},
    status: { type: Number, default: 1},
    borrado: { type:Boolean, default: false},
    fecha_creacion: { type:Date, default: Date.now},
    fecha_modificacion: { type:Date, default: null}


})

// status
// 0 - rechazada
// 1 - enviada
// 2 - aceptada

module.exports = mongoose.model('postulaciones', serviciosSchema);

