const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let serviciosSchema = new Schema({

    usuario: { type: Schema.Types.ObjectId, ref:"usuario", default: null},
    nombre: { type: String, required: true},
    descripcion: { type:String, required: true},
    status: { type: Number, default: 0},
    borrado: { type:Boolean, default: false},
    fecha_creacion: { type:Date, default: Date.now},
    fecha_modificacion: { type:Date, default: null},
    fecha_evento: { type:Date, default: null},
    lugar_evento: { type:String, default: null},
    max_price: { type:Number, required: true},
    min_price: { type:Number, required: true},
    categoria: { type:Schema.Types.ObjectId, ref: 'categoria'},
    subcategoria: { type:Schema.Types.ObjectId, ref: 'subcategoria'},
    cotizacion: { type:Schema.Types.ObjectId, ref: 'cotizaciones', default:null,},
    idPayku: {type: String, default: null},
    
})
// status
// 0 - enviada
// 1 - cotizada
// 2 - proceso
// 3 - finalizada
// 4 - cancelada

module.exports = mongoose.model('servicios', serviciosSchema);

