var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var calificacionSchema = new Schema({
    usuario: {type: Schema.Types.ObjectId, required: true, ref: 'usuario'},
    puntaje: {type: Number, required: true},
    comentario: {type: String},
    borrado: {type: Boolean, default: false},
    fechaCreacion: {type:Date, default:Date.now}
})

module.exports = mongoose.model('calificacion', calificacionSchema);