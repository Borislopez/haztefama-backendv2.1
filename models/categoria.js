var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var categoriaSchema = new Schema({

    nombre: {type: String, required: true }, 
    status: {type: Boolean, default: true },
    code:{ type: String ,  unique: true , required: true },
    borrado: {type:Boolean,default:false},
    fecha_creacion:{type:Date,default:Date.now},
    fecha_modificacion:{type:Date,default:null}

});

module.exports = mongoose.model('categoria', categoriaSchema);