var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var usuarioSchema = new Schema({
    nombre: { type: String, required: true },
    apellido: { type: String,default:'' },
    email: { type: String, required: true, unique: true },
    username: { type: String, unique: true,required:true },
    password: { type: String },
    telefono: { type: String },
    categoria: { type: Schema.Types.ObjectId , ref: 'categoria' , default: null,},
    direccion: { type: String },
    role: { type: String , enum:['admin','client','provider'] },
    roleAdmin: {type: Schema.Types.ObjectId , ref: 'roles', default: null},
    status: {type:Boolean, default: true},
    fechaCreacion: {type:Date, default:Date.now},
    ubicacion: {type:String, default:null},
    recovery_password: {type:Boolean, default:false},
    recovery_password_token: {type:String, default:null},
    verificado: {type:Boolean, default:false},
    token_verificacion: {type:String, default:null},
    simpleAccount:{type:Boolean, default:false},
    socialAccount:{type:Boolean, default:false},
    uid:{type:String,default:null},
    fecha_modificacion:{type:Date,default:null},
    foto:{type:String,default:'profile.jpg'},
    creditos:{type:Number,default:0},
    borrado: {type:Boolean,default:false}
});

module.exports = mongoose.model('usuario', usuarioSchema);