const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const contratoSchema = new Schema({

    titulo: {type: String, required: true},
    contrato: {type: String, required: true},
    destino: {type: String, enum:['admin','client','provider'], unique: true},
    description: {type: String},
    status: {type: Number, default: 0},
    borrado: {type: Boolean, default: false},
    fecha_creacion: {type: Date, default: Date.now },
    fecha_modificacion:{type:Date,default:null},
})

// status
// 0 - Rechazada
// 1 - Enviada
// 2 - Aceptada 

module.exports = mongoose.model('contrato', contratoSchema)